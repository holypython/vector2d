from vector2d import Vector2


class VectorLike:
    x = 2
    y = 3


v1 = Vector2(2, 2)
v2 = Vector2(-2, 0)
t1 = (1, 2)

v3 = 2*v2 + v1

v3 += (2,3)

v3 += VectorLike

print(v3)
a,b = v3


v3 = 2*v1 + t1 - v2 # basic operators works
assert (v3 == (7, 6))


v3 += (0, -2)  # tuples can me used as Vector (duck typing)
assert (v3 == (7, 4))  # BTW, I use this trick here to test equality

x, y = v3             # semantic is equivalent to `x = c[0]; y = c[1]`
assert (x == v3.x)       # vector2 has x and y properties
assert (y == v3[1])     # vector2 are iterables

v3 -= VectorLike  # VectorLike has x and y properties. Hence duck-typing kicks in
assert (v3 == (5, 1))

d = (1, 2)
d += v3  # test of reverse inplace operator   WARNING: this turn d into a Vector2
assert (d == (6, 3))

